# -*- coding: utf-8 -*-

from functions import *
import matplotlib.pyplot as plt
import tensorflow as tf
from time import gmtime, strftime

imgCount = 4

# ----- load --------
test_data = np.load('../data/train.npz')
testX = test_data['arr_1']
testY = test_data['arr_0']
indexes = random.sample(range(0, len(testX)), imgCount)
testX = testX[indexes]
testY = testY[indexes]
print(testX.shape)
print(indexes)

testX = zeroOneScaleColoursInImagesTest(testX)
print(len(testX))

# numImageRows = 1 + (len(trainY) // 10)
# plt.figure(figsize=(50, 5 * numImageRows))
# for imgID in range(len(trainY)):
#     plt.subplot(numImageRows, 10, imgID + 1)
#     plt.imshow(trainY[imgID])
# plt.show()

# --------------------------------------- GRAPH DEFINITION -------------------------------------------------------------
inputLayer, outputLayer, resized = defineGraph(testX.shape)
# ----------------------------------------------------------------------------------------------------------------------


# calculate the loss and optimize the network
originalInputImage = tf.placeholder(tf.float32, (None, 250, 250, 3))

# initialize the network
init = tf.global_variables_initializer()

session = tf.Session()
session.run(init)
loadSession(session, '2018-04-29-17-14')
writer = tf.summary.FileWriter("../plots/graph", session.graph)
lossResized = tf.reduce_mean(tf.square(outputLayer - resized))
[outputTstImgs, outResized, resizedloss] = session.run([outputLayer, resized, lossResized],
                                                       feed_dict={inputLayer: testX, originalInputImage: testY})
outputTstImgs = backScaleColoursInImages(outputTstImgs)
inputTstImgs = backScaleColoursInImages(testX)

print('Resized mean error: ' + str(resizedloss))

for imgID in range(len(outputTstImgs)):
    plt.figure(dpi=200)
    plt.subplot(2, 2, 1)
    plt.imshow(inputTstImgs[imgID])
    plt.xticks(())
    plt.yticks(())
    plt.subplot(2, 2, 2)
    plt.imshow(outputTstImgs[imgID])
    plt.xticks(())
    plt.yticks(())
    plt.subplot(2, 2, 3)
    plt.imshow(outResized[imgID])
    plt.xticks(())
    plt.yticks(())
    plt.subplot(2, 2, 4)
    plt.imshow(testY[imgID])
    plt.xticks(())
    plt.yticks(())
    plt.tight_layout(pad=0.01, w_pad=0.01, h_pad=0.01)
    plt.subplots_adjust(top=0.975,
                        bottom=0.03,
                        left=0.158,
                        right=0.877,
                        hspace=0.022,
                        wspace=0.0)
    plt.savefig('../plots/validation' + str(imgID) + '.pdf', format='pdf', bbox_inches='tight')
    plt.show()
writer.close()