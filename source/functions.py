# -*- coding: utf-8 -*-
from scipy import ndimage
import random as random
import os as os
import itertools as itertools
import numpy as numpy
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

basePath = "../media/"
downsamplePath = "../downsampled_media/"


# Load the images and store them if necessary.
def readSingleImage(fileName):
    return (ndimage.imread(fileName, mode="RGB"))


def unison_shuffle(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]


def defineGraph(inShape):
    tf.reset_default_graph()
    print(inShape)
    inputLayer = tf.placeholder(tf.float32, (None, inShape[1], inShape[2], 3))
    bilResize = tf.image.resize_bilinear(
        inputLayer,
        size=(250, 250),
        align_corners=False,
        name=None
    )
    deconv0 = tf.contrib.layers.conv2d_transpose(inputs=inputLayer,
                                                 num_outputs=6,
                                                 kernel_size=2,
                                                 stride=2,
                                                 padding="SAME")
    deconv1 = tf.contrib.layers.conv2d_transpose(inputs=deconv0,
                                                 num_outputs=6,
                                                 kernel_size=4,
                                                 stride=2,
                                                 padding="SAME")
    deconv2 = tf.contrib.layers.conv2d_transpose(inputs=deconv1,
                                                 num_outputs=6,
                                                 kernel_size=8,
                                                 stride=2,
                                                 padding="SAME")
    deconv3 = tf.contrib.layers.conv2d_transpose(inputs=deconv2,
                                                 num_outputs=6,
                                                 kernel_size=8,
                                                 stride=2,
                                                 padding="SAME")
    outputLayer = tf.contrib.layers.conv2d(inputs=deconv3,
                                           num_outputs=3,
                                           kernel_size=8,
                                           stride=8,
                                           padding="SAME")
    print(inputLayer.shape)
    print(outputLayer.shape)

    return inputLayer, outputLayer, bilResize

def defineGraphReturnAll(inShape):
    tf.reset_default_graph()
    print(inShape)
    inputLayer = tf.placeholder(tf.float32, (None, inShape[1], inShape[2], 3))
    bilResize = tf.image.resize_bilinear(
        inputLayer,
        size=(250, 250),
        align_corners=False,
        name=None
    )
    deconv0 = tf.contrib.layers.conv2d_transpose(inputs=inputLayer,
                                                 num_outputs=6,
                                                 kernel_size=2,
                                                 stride=2,
                                                 padding="SAME")
    deconv1 = tf.contrib.layers.conv2d_transpose(inputs=deconv0,
                                                 num_outputs=6,
                                                 kernel_size=4,
                                                 stride=2,
                                                 padding="SAME")
    deconv2 = tf.contrib.layers.conv2d_transpose(inputs=deconv1,
                                                 num_outputs=6,
                                                 kernel_size=8,
                                                 stride=2,
                                                 padding="SAME")
    deconv3 = tf.contrib.layers.conv2d_transpose(inputs=deconv2,
                                                 num_outputs=6,
                                                 kernel_size=8,
                                                 stride=2,
                                                 padding="SAME")
    outputLayer = tf.contrib.layers.conv2d(inputs=deconv3,
                                           num_outputs=3,
                                           kernel_size=8,
                                           stride=8,
                                           padding="SAME")
    print(inputLayer.shape)
    print(outputLayer.shape)

    return inputLayer, outputLayer, bilResize, deconv0 ,deconv1,deconv2,deconv3

def loadPicturesFromFiles2(basePath , numFaces):
    allNames = random.sample(os.listdir(basePath), numFaces) if numFaces else os.listdir(basePath)
    originalPictures = {}
    for personName in allNames:
        personPictureDirectory = os.path.join(basePath, personName)
        if (not (personName[0] == ".")) & os.path.isdir(personPictureDirectory):
            print("Reading faces of " + personName + "...", end="")
            pictureFiles = os.listdir(personPictureDirectory)
            pictureFiles = list(map(os.path.join, [personPictureDirectory] * len(pictureFiles), pictureFiles))
            pictures = list(map(readSingleImage, pictureFiles))
            print(" DONE (" + str(len(pictures)) + " read)")
            originalPictures[personName] = pictures
    return originalPictures

def loadPicturesFromFiles(basePath, downsampledPath , numFaces):
    allNames = random.sample(os.listdir(basePath), numFaces) if numFaces else os.listdir(basePath)
    originalPictures = {}
    smallPictures = {}
    for personName in allNames:
        personPictureDirectory = os.path.join(basePath, personName)
        if (not (personName[0] == ".")) & os.path.isdir(personPictureDirectory):
            print("Reading faces of " + personName + "...", end="")
            pictureFiles = os.listdir(personPictureDirectory)
            pictureFiles = list(map(os.path.join, [personPictureDirectory] * len(pictureFiles), pictureFiles))
            pictures = list(map(readSingleImage, pictureFiles))
            print(" DONE (" + str(len(pictures)) + " read)")
            originalPictures[personName] = pictures

        personPictureDirectory = os.path.join(downsampledPath, personName)
        if (not (personName[0] == ".")) & os.path.isdir(personPictureDirectory):
            print("Reading faces of " + personName + "...", end="")
            pictureFiles = os.listdir(personPictureDirectory)
            pictureFiles = list(map(os.path.join, [personPictureDirectory] * len(pictureFiles), pictureFiles))
            pictures = list(map(readSingleImage, pictureFiles))
            print(" DONE (" + str(len(pictures)) + " read)")
            smallPictures[personName] = pictures
    return originalPictures, smallPictures


def splitImagesToTrainingAndTestingSets(allImagesDict, trainingPortion=0.75):
    trainingImages = []
    trainingLabels = []
    testingImages = []
    testingLabels = []
    for personNames, pictures in allImagesDict.items():
        if (random.uniform(0, 1) < trainingPortion):
            trainingLabels.append([personNames] * len(pictures))
            trainingImages.append(pictures)
        else:
            testingLabels.append([personNames] * len(pictures))
            testingImages.append(pictures)
    return ((
        list(itertools.chain(*trainingImages)),
        list(itertools.chain(*trainingLabels)),
        list(itertools.chain(*testingImages)),
        list(itertools.chain(*testingLabels))))


def splitImagesToTrainingAndTestingSets2(X, Y, trainingPortion=0.75):
    trainX = []
    testX = []
    trainY = []
    testY = []
    for person, pictures in Y.items():
        if (random.uniform(0, 1) < trainingPortion):
            trainX.append(X.get(person))
            trainY.append(pictures)
        else:
            testX.append(X.get(person))
            testY.append(pictures)
    return ((
        list(itertools.chain(*trainX)),
        list(itertools.chain(*trainY)),
        list(itertools.chain(*testX)),
        list(itertools.chain(*testY))))

def getTestImages(X, Y):
    testX = []
    testY = []
    for person, pictures in Y.items():
            testX.append(X.get(person))
            testY.append(pictures)
    return ((
        list(itertools.chain(*testX)),
        list(itertools.chain(*testY))))


def expandSingleImage(img):
    expandedImage = numpy.zeros((256, 256, 3), dtype=numpy.uint8)
    expandedImage[:img.shape[0], :img.shape[1], :] = img
    return (expandedImage)


def expandAllImages(trainingImages, testingImages):
    trainingImages = list(map(expandSingleImage, trainingImages))
    testingImages = list(map(expandSingleImage, testingImages))
    return ((trainingImages, testingImages))


def zeroOneScaleColoursInImages(trainingImages, testingImages):
    trainingImages = np.array(list(map(lambda image: image / 255.0, trainingImages)))
    testingImages = np.array(list(map(lambda image: image / 255.0, testingImages)))
    return ((trainingImages, testingImages))

def zeroOneScaleColoursInImagesTest(imgs):
    imgs = np.array(list(map(lambda image: image / 255.0, imgs)))
    return imgs


def backScaleColoursInImages(imageList):
    m = lambda x: min(x, 255)
    mmin = numpy.vectorize(m)
    imageList = list(map(lambda image: mmin(image * 255), imageList))
    imageList = list(map(lambda image: image.astype(numpy.uint8), imageList))
    return (imageList)


def showProgressOnTestingImages(inputTstImgs, outputTstImgs):
    inputTstImgs = backScaleColoursInImages(inputTstImgs)
    outputTstImgs = backScaleColoursInImages(outputTstImgs)

    plt.figure(figsize=(7.5, 2.5 * len(inputTstImgs)))
    for imgID in range(len(inputTstImgs)):
        plt.subplot(len(inputTstImgs) + 1, 3, imgID * 3 + 1)
        plt.imshow(inputTstImgs[imgID])

        plt.subplot(len(inputTstImgs) + 1, 3, imgID * 3 + 3)
        plt.imshow(outputTstImgs[imgID])
    plt.show()


def saveSession(session, name):
    saver = tf.train.Saver()
    dir = '../models/' + str(name)
    if not os.path.exists(dir):
        os.makedirs(dir)
    saver.save(session, dir + '/model.ckpt')


def loadSession(session, name):
    saver = tf.train.Saver()
    dir = '../models/' + str(name)
    saver.restore(session, dir + '/model.ckpt')


def randomShuffleImages(inputImages, noisedInputImages):
    idx = (random.sample(range(len(inputImages)), len(inputImages)))
    inputImages = list(map(inputImages.__getitem__, idx))
    noisedInputImages = list(map(noisedInputImages.__getitem__, idx))
    return (inputImages, noisedInputImages)
