# -*- coding: utf-8 -*-

from functions import *
import matplotlib.pyplot as plt
import tensorflow as tf
from time import gmtime, strftime

print('Start time: '+str(strftime("%Y-%m-%d-%H-%M", gmtime())))

# ----- save --------
# imgCount = 1000
# resultImagesDict, downsampledImagesDict = loadPicturesFromFiles(downsamplePath, basePath, imgCount)
#
# [trainX, trainY, testX, testY] = splitImagesToTrainingAndTestingSets2(downsampledImagesDict, resultImagesDict)
#
# np.savez_compressed('../data/train', trainX, trainY)
# np.savez_compressed('../data/test', testX, testY)
# exit(1)

# ----- load --------
train_data = np.load('../data/train.npz')
test_data = np.load('../data/test.npz')
trainX = train_data['arr_1'][0:1000]
trainY = train_data['arr_0']
testX = test_data['arr_1'][0:1000]
testY = test_data['arr_0']

# [trainY, testY] = expandAllImages(trainY, testY)
[trainY, testY] = zeroOneScaleColoursInImages(trainY, testY)
[trainX, testX] = zeroOneScaleColoursInImages(trainX, testX)
print(trainX.shape)
print(trainY.shape)
print(testX.shape)
print(testY.shape)
# numImageRows = 1 + (len(trainY) // 10)
# plt.figure(figsize=(50, 5 * numImageRows))
# for imgID in range(len(trainY)):
#     plt.subplot(numImageRows, 10, imgID + 1)
#     plt.imshow(trainY[imgID])
# plt.show()

# --------------------------------------- GRAPH DEFINITION -------------------------------------------------------------
inputLayer, outputLayer, resized = defineGraph(trainX.shape)
# ----------------------------------------------------------------------------------------------------------------------


# calculate the loss and optimize the network
originalInputImage = tf.placeholder(tf.float32, (None, 250, 250, 3))
lossFunction = tf.reduce_mean(tf.square(outputLayer - originalInputImage))  # calaculate the mean square error loss
global_step = tf.Variable(0, trainable=False)
starter_learning_rate = 0.001
learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,
                                           8, 0.99, staircase=True)
optimisation = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(lossFunction, global_step=global_step)
# initialize the network
init = tf.global_variables_initializer()
session = tf.Session()
session.run(init)
testL = session.run([lossFunction], feed_dict={inputLayer: testX[0:8], originalInputImage: testY[0:8]})
print("Initial test loss: " + str(testL))
for epochID in range(40):
    p = numpy.random.permutation(len(trainX))
    epochTrainingImages = trainX[p]
    originalTrainingImages = trainY[p]
    # batchLen = len(epochTrainingImages) // 200  # Be careful here if you have large number of images.
    batchLen = 4  # Be careful here if you have large number of images.
    trainLoss = 0
    print("Running epoch {}".format(epochID), end="")
    for batchID in range(batchLen):
        batchImages = epochTrainingImages[batchID * batchLen: min((batchID + 1) * batchLen, len(epochTrainingImages))]
        batchOriginalImages = originalTrainingImages[
                              batchID * batchLen: min((batchID + 1) * batchLen, len(originalTrainingImages))]
        if len(batchImages) == 0:
            continue
        _, trainLoss = session.run([optimisation, lossFunction],
                                   feed_dict={inputLayer: batchImages, originalInputImage: batchOriginalImages})
        print(".", end="")

    testLoss = session.run([lossFunction], feed_dict={inputLayer: testX[0:2*batchLen], originalInputImage: testY[0:2*batchLen]})
    print(" loss on testing was {}".format(testLoss), end="")
    print(" loss on last batch was {}".format([trainLoss]), end="")
    print(" learning rate is was {}".format(learning_rate.eval(session=session)))
saveSession(session, strftime("%Y-%m-%d-%H-%M", gmtime()))