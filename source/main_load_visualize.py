# -*- coding: utf-8 -*-

from functions import *
import matplotlib.pyplot as plt
import tensorflow as tf
from time import gmtime, strftime
imgCount = 1

# ----- load --------
test_data = np.load('../data/train.npz')
testX = test_data['arr_1']
testY = test_data['arr_0']
indexes = random.sample(range(0, len(testX)), imgCount)
testX = testX[indexes]
testY = testY[indexes]
print(testX.shape)
print(indexes)

testX = zeroOneScaleColoursInImagesTest(testX)
print(len(testX))

# numImageRows = 1 + (len(trainY) // 10)
# plt.figure(figsize=(50, 5 * numImageRows))
# for imgID in range(len(trainY)):
#     plt.subplot(numImageRows, 10, imgID + 1)
#     plt.imshow(trainY[imgID])
# plt.show()

# --------------------------------------- GRAPH DEFINITION -------------------------------------------------------------
inputLayer, outputLayer, resized, deconv0, deconv1, deconv2, deconv3 = defineGraphReturnAll(testX.shape)
# ----------------------------------------------------------------------------------------------------------------------


# calculate the loss and optimize the network
originalInputImage = tf.placeholder(tf.float32, (None, 250, 250, 3))

# initialize the network
init = tf.global_variables_initializer()

session = tf.Session()
session.run(init)
loadSession(session, '2018-04-29-17-14')
lossResized = tf.reduce_mean(tf.square(outputLayer - resized))
[outputTstImgs, d0, d1, d2, d3] = session.run([outputLayer, deconv0, deconv1, deconv2, deconv3],
                                              feed_dict={inputLayer: testX, originalInputImage: testY})
outputTstImgs = backScaleColoursInImages(outputTstImgs)
inputTstImgs = backScaleColoursInImages(testX)
d0 = np.delete(d0,numpy.s_[3:6],3)
d1 = np.delete(d1,numpy.s_[3:6],3)
d2 = np.delete(d2,numpy.s_[3:6],3)
d3 = np.delete(d3,numpy.s_[3:6],3)

d0 = backScaleColoursInImages(d0)
d1 = backScaleColoursInImages(d1)
d2 = backScaleColoursInImages(d2)
d3 = backScaleColoursInImages(d3)


for imgID in range(len(outputTstImgs)):
    plt.figure(dpi=200)

    plt.subplot(2, 4, 1)
    plt.imshow(inputTstImgs[imgID])
    plt.xticks(())
    plt.yticks(())

    plt.subplot(2, 4, 2)
    plt.imshow(d0[imgID])
    plt.xticks(())
    plt.yticks(())

    plt.subplot(2, 4, 3)
    plt.imshow(d1[imgID])
    plt.xticks(())
    plt.yticks(())

    plt.subplot(2, 4, 4)
    plt.imshow(d2[imgID])
    plt.xticks(())
    plt.yticks(())

    plt.subplot(2, 4, 5)
    plt.imshow(d3[imgID])
    plt.xticks(())
    plt.yticks(())

    plt.subplot(2, 4, 6)
    plt.imshow(outputTstImgs[imgID])
    plt.xticks(())
    plt.yticks(())

    plt.subplot(2, 4, 7)
    plt.imshow(testY[imgID])
    plt.xticks(())
    plt.yticks(())

    # plt.tight_layout(pad=0.01, w_pad=0.01, h_pad=0.01)
    # plt.subplots_adjust(top=0.975,
    #                     bottom=0.03,
    #                     left=0.158,
    #                     right=0.877,
    #                     hspace=0.022,
    #                     wspace=0.0)
    plt.savefig('../plots/vizualize' + str(imgID) + '.pdf', format='pdf', bbox_inches='tight')
    plt.show()
