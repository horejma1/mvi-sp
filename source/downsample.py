# -*- coding: utf-8 -*-
from skimage.transform import resize
from skimage.io import imsave

from functions import *

allImagesDict = loadPicturesFromFiles2(basePath, False)
# print(allImagesDict)

for person, pictures in allImagesDict.items():
    print(person)
    personDir = downsamplePath + person
    if not os.path.exists(downsamplePath + person):
        os.makedirs(personDir)
    i = 0
    for picture in pictures:
        i = i + 1
        small_picture = resize(picture, (125, 125))
        imsave(personDir + "/" + str(person) + "_" + str(i).zfill(4) + ".jpg", small_picture)
