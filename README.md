# Super-resolution Tensorflow
Cílem této práce je implementovat metodu zvýšení rozlišení obrázků pomocí dekonvoluční neuronové sítě.
Výsledek by měl být lepší než běžné zvětšení obrázků pomocí pixelace nebo vyhlazování pixelů.

Jako dataset jsem zvolil sadu obrázků LFW: http://vis-www.cs.umass.edu/lfw/lfw.tgz 

Práce je naimplementovaná ve frameworku Tensorflow.

## Struktura projektu
- /source zdrojové soubory implementace
   - /functions.py implementace funkcí a grafu neuronové sítě
   - /downsample.py generace zmenšenin (vstupů do sítě) z originálních obrázků
   - /main.py spustí trénování sítě
   - /main_load.py načte vybraný uložený model (složka z /models) trénování a zobrazí výstupy náhodných obrázků
- /models exportované naučené modely sítě v tensorflow
- /media extrahovaný dataset LFW
- /downsampled_dataset dataset LFW downsamplovaný na poloviční rozlišení 
- /plots uložené poslední 4 výstupy z main_load.py
- /data uložený trénovací a validační dataset pomocí numpy.savez_compressed()
- /report.pdf report generovaný šablonou latexu

### Prostředí
- Python 3.6
- Tensorflow 1.7.0
- Windows 10 Home x64
- 8GB RAM
- Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz
- NVIDIA GeForce MX150 4GB GDDR5
- CUDA 9.0
- cuDNN 7

